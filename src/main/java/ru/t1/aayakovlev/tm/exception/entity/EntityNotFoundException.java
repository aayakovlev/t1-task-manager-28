package ru.t1.aayakovlev.tm.exception.entity;

public final class EntityNotFoundException extends AbstractEntityException {

    public EntityNotFoundException() {
        super("Error! Record not found...");
    }

}
